<?php

class emuTheme extends emuApp
{
    function config()
    {
        $this->emuAppID = 'emuTheme';
        $this->menuName = 'Emu Theme';
        $this->dbPrefix = 'emu_theme_';
    }

    function init()
    {
        $this->loadManager('theme');
    }

    function loadCoreStyles()
    {
        wp_enqueue_style('960gs', $this->sThemeURL.'/css/960.css');
    }

}


?>