<?php

class emuM_Theme extends emuManager
{
    public function init()
    {
        add_theme_support( 'post-thumbnails' );

        add_action( 'after_setup_theme', array( $this, 'setupMenus' ) );
        add_action( 'init', array( $this, 'registerSideBars') );

        // $this->registerProcessorClass( 'ref', 'processorClassName', 'process.example.class.php' );
		// $this->registerProcessorFunction( 'ref', array($this, 'processFunction') );
        // $this->registerAjaxAction( 'ref', array( $this, 'AJAX_Function' ) );

	}

    // public function processFunction()
    // {
    // }

    // public function AJAX_Function()
    // {
    // }

    // public function registerClasses()
    // {
    // 	$this->emuApp->registerClass( 'className', 'example.class.file.php' );
    // }

	// public function loadStyles()
	// {
    // 		wp_enqueue_style( 'example', $this->emuApp->pluginURL.'/css/example.css' );
	// }

	// public function loadScripts()
	// {
	// 		wp_enqueue_script('example', $this->emuApp->pluginURL.'/js/example.js', array( 'jquery' ));
	// }


    function registerSideBars()
    {
        register_sidebar( array( 'name'          => 'Header',
                                 'id'            => 'header',
                                 'description'   => '',
                                 'before_widget' => '',
                                 'after_widget'  => '',
                                 'before_title'  => '',
                                 'after_title'   => '' ) );
    }

}

?>